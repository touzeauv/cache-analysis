#include <otawa/app/Application.h>
#include <otawa/prog/Manager.h>
#include <otawa/proc/ProcessorPlugin.h>
#include <otawa/prog/WorkSpace.h>

#include <lrumc/features.h>

#include <elm/data/List.h>

using namespace elm;
using namespace otawa;

enum Analysis
{
	ZDD = 0,
	SHARING_ZDD = 1,
	AGE_BASED = 2,
	AGE_BASED_DU = 3,
	AGE_BASED_DU_MC = 4,
	AGE_BASED_DU_ZDD = 5,
	AGE_BASED_DU_SHARING_ZDD = 6,
	SECURITY = 7,
	SECURITY_OVER = 8,
	SECURITY_UNDER = 9
};


class OCache: public Application {
public:
	OCache(void):
		Application("ocache", Version(0, 1, 0), "cache analysis playground"),
		//ids(option::ListOption<cstring>::Make(*this).cmd("-p").cmd("--prop").description("select which property to output").argDescription("ID")),
		_mc_opt(option::ValueOption<string>::Make(*this).cmd("-n").cmd("--nuxmv").description("path to nuXmv model checker").argDescription("NUXMV_PATH")),
		_analysis_opt(option::ValueOption<int>::Make(*this).cmd("-a").cmd("--analysis").usage(option::arg_required).description(
				"Select analysis to perform\n"
				"\t\t 0. ZDD\n"
				"\t\t 1. Sharing ZDD\n"
				"\t\t 2. Age based\n"
				"\t\t 3. Age based + Definitely Unknown\n"
				"\t\t 4. Age based + Definitely Unknown + Model Checker\n"
				"\t\t 5. Age based + Definitely Unknown + ZDD\n"
				"\t\t 6. Age based + Definitely Unknown + Sharing ZDD\n"
				"\t\t 7. Security Analysis"
				"\t\t 8. Overapproximated Security Analysis"
			).argDescription("ANALYSIS")),
		_cache_opt(option::ValueOption<string>::Make(*this).cmd("-c").cmd("--cache").description("select cache configuration file").argDescription("CACHE_CONFIG")),
        _inlining(option::Switch::Make(*this).cmd("-i").cmd("--inline").description("enable inlining"))
//		ferdinand(option::Switch::Make(*this).cmd("--ferdinand").description("perform usual May/Must analysis")),
//		model_checking(option::Switch::Make(*this).cmd("--model-checking").description("perform exact analysis using model checking")),
//		zdd(option::Switch::Make(*this).cmd("--zdd").description("perform exact analysis using ZDDs"))
		//analysis(*this, 'a', "--analysis", "select the cache analysis to perform", {{}, {}, {"",I}})
		//no_insts(option::Switch::Make(*this).cmd("-I").cmd("--no-insts").description("do not include instructions in output")),
		//dot(option::Switch::Make(*this).cmd("-D").cmd("--dot").description("select .dot output")),
		//phony(option::Switch::Make(*this).cmd("-P").cmd("--phony").description("do not perform any output"))
	{
	}

protected:

	virtual void prepare(PropList &props) {
//		for(int i = 0; i < ids.count(); i++)
//			cfgio::INCLUDE(props).add(&ids[i]);
//		if(no_insts)
//			cfgio::NO_INSTS(props) = true;


		int analysis_value = _analysis_opt.get();
		if(analysis_value < 0 || analysis_value > 9) // TODO remove magic number
			throw otawa::Exception(_ << "Invalid analysis number. See ocache --help for more information");
		else
			_analysis = static_cast<enum Analysis>(analysis_value);
//		if((ferdinand && (model_checking || zdd)) ||
//		   (model_checking && (ferdinand || zdd)) ||
//		   (zdd && (ferdinand || model_checking))) {
//			throw otawa::Exception(_ << "Choose only one analysis to perform");
//		}
//
//		if(!ferdinand && !model_checking && !zdd)
//			throw otawa::Exception(_ << "Choose one analysis to perform");


		if(_cache_opt) {
			Path path = *_cache_opt;
			CACHE_CONFIG_PATH(props) = path;
		}
		else {
			throw otawa::Exception(_ << "No cache configuration given.");
		}

		//lrumc::NUXMV_PATH(props) = "/local/nuXmv-1.1.0-Linux/bin/nuXmv";

		if(_analysis == AGE_BASED_DU_MC) {
			if(!_mc_opt) {
				throw otawa::Exception(_ << "Path to nuXmv should be provided when using model checking based analysis.");
			}
			setStringIdentifier(props, "lrumc::CACHE_MODEL_KIND", lrumc::CacheModelKind::YOUNGER_SET);
			setStringIdentifier(props, "lrumc::NUXMV_PATH", _mc_opt.get());
			setStringIdentifier(props, "lrumc::NUXMV_WORKSPACE_PATH", string("/tmp/"));
		}

	}

	virtual void work(PropList& props) throw(elm::Exception)
	{
		const Vector<string> &args = arguments();

		for(int i = 0; i < args.count(); i++) {
			string a = args[i];
			if(!setTask(props, a))
				throw otawa::Exception(_ << "unexpected positional arguments \"" << a << "\".");
		}

        // Enable Inlining
        if(_inlining)
            execProcessor(props, "otawa::Virtualizer");

		// Perform May/Must analysis
		if(_analysis >= AGE_BASED) {
			execProcessor(props, "lrupreanalysis::MustAnalysis");
			execProcessor(props, "lrupreanalysis::MayAnalysis");
			execProcessor(props, "lrupreanalysis::CatBuilder");
		}

		// Perform Definitely Unknown analysis
		if(_analysis >= AGE_BASED_DU) {
			requireFeature(props, "lrumc::DU_CATEGORY_FEATURE");
		}


		// Perform program simplification based on May analysis
		// Perform Model Checking analysis
		if(_analysis == AGE_BASED_DU_MC) {
			requireFeature(props, "lrumc::LBLOCK_SLICING_PROGRAM_MODEL_FEATURE");
			requireFeature(props, "lrumc::MODEL_CHECKING_ANALYSIS_FEATURE");
		}

		// Perforn Sharing ZDD analysis
		if(_analysis == SHARING_ZDD || _analysis == AGE_BASED_DU_SHARING_ZDD) {
			execProcessor(props, "lruzdd::GlobalMayAnalysis");
			execProcessor(props, "lruzdd::GlobalMustAnalysis");
		}

		// Perforn ZDD analysis (if not sharing zdd) and classify blocks
		if(_analysis == ZDD || _analysis == SHARING_ZDD || _analysis == AGE_BASED_DU_ZDD || _analysis == AGE_BASED_DU_SHARING_ZDD || _analysis == SECURITY || _analysis == SECURITY_OVER || _analysis == SECURITY_UNDER)
			execProcessor(props, "lruzdd::ClassificationBuilder");

		// Perform Classification
		if(_analysis == AGE_BASED || _analysis == AGE_BASED_DU)
			execProcessor(props, "lrupreanalysis::MayMustClassificationBuilder");

		if(_analysis == AGE_BASED_DU)
			execProcessor(props, "lrumc::DUToExactCatBuilder");

		if(_analysis == SECURITY) {
			execProcessor(props, "lrusecurity::ExistHitPrevAnalysis");
			execProcessor(props, "lrusecurity::SecurityCatBuilder");
		}
		if(_analysis == SECURITY_OVER) {
			execProcessor(props, "lrusecurity::ExistHitPrevOAnalysis");
			execProcessor(props, "lrusecurity::SecurityCatBuilder");
		}
		if(_analysis == SECURITY_UNDER)
		{
			execProcessor(props, "lrusecurity::ExistHitPrevUAnalysis");
			execProcessor(props, "lrusecurity::SecurityCatBuilder");
		}
		//Display results
		if (_analysis < SECURITY)
			execProcessor(props, "lruexact::ClassificationDisplayer");
		if (_analysis >= SECURITY){
			execProcessor(props, "lrusecurity::SecurityDisplayer");
		}
		
	}

	template <typename Value>
	void setStringIdentifier(PropList& props, string id, Value value)
	{
		//lrumc::NUXMV_PATH(props) = "/local/nuXmv-1.1.0-Linux/bin/nuXmv";
		string identifier_name = id;
		Identifier<Value>* identifier = dynamic_cast<Identifier<Value>*>(ProcessorPlugin::getIdentifier(&identifier_name));
		if(identifier)
			(*identifier)(props) = value;
		else
			throw otawa::Exception(_ << "cannot find identifier " << identifier_name);
	}

	void execProcessor(PropList& props, string name)
	{
		string processor_name = name;
		Processor* processor = ProcessorPlugin::getProcessor(&processor_name);
		if(processor) {
			workspace()->run(processor, props, true);
			processor_list.add(processor);
		}
		else {
			clean();
			throw otawa::Exception(_ << "cannot find processor " << processor_name);
		}
	}

	void requireFeature(PropList& props, string name)
	{
		string feature_name = name;
		AbstractFeature* feature = ProcessorPlugin::getFeature(&feature_name);
		if(feature) {
			workspace()->require(*feature, props);
		}
		else {
			clean();
			throw otawa::Exception(_ << "cannot find feature " << feature_name);
		}
	}

	void clean()
	{
		for(List<Processor*>::Iter it(processor_list); it(); ++it)
			delete processor_list.at(it);

		processor_list.clear();
	}

	void complete(PropList&) throw(elm::Exception) {
//		if(!workspace()->isProvided(COLLECTED_CFG_FEATURE))
//			workspace()->require(COLLECTED_CFG_FEATURE, props);
//
//		// if enabled, perform output
//		if(!phony) {
//
//			// XML output
//			if(!dot) {
//				if(out)
//					cfgio::OUTPUT(props) = *out;
//				cfgio::Output output;
//				output.process(workspace(), props);
//			}
//
//			// DOT output
//			else {
//				if(out)
//					display::CFGOutput::PATH(props) = *out;
//				display::CFGOutput::KIND(props) = display::OUTPUT_DOT;
//				display::CFGOutput output;
//				output.process(workspace(), props);
//			}
//		}
	}

private:
	bool setTask(PropList& props, string entry) {
		if(task)
			return false;
		else {
			task = entry;
			TASK_ENTRY(props) = entry;
			return true;
		}
	}

	string task;
	//option::ListOption<string> ids;
	option::ValueOption<string> _mc_opt;
	option::ValueOption<int> _analysis_opt;
	option::ValueOption<string> _cache_opt;
	List<Processor*> processor_list;

	enum Analysis _analysis;
    option::Switch _inlining;
//	option::Switch ferdinand;
//	option::Switch model_checking;
//	option::Switch zdd;
	//option::Switch dot, phony;
};

OTAWA_RUN(OCache);
