# Welcome on the homepage of the Cache Analysis Plugin for Otawa !

The aim of this plugin is to provide an efficient and optimally precise analysis of LRU caches in Otawa.
All the documentation is currently being rewritten.
You will find the most up-to-date information on the [wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/touzeauv/cache-analysis/-/wikis/Home) of this project.
If you do not find what you are looking for, you can try the older version of the documentation in the doc directory of this repository.
Finally, you can also contact me at [Valentin.Touzeau@cs.uni-saarland.de](mailto:valentin.touzeau@cs.uni-saarland.de).
