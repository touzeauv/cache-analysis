#!/bin/bash

RESULTS_DIR="results"



SCRIPT_DIR="$(cd "$(dirname "$0")" ; pwd -P)"
WCET_INFO_SCRIPT="$SCRIPT_DIR/wcet_info.py"

LOG_LIST=$(find "$RESULTS_DIR" -name "*.log")

echo "bench,inline,unroll,analysis,optim,wcet,cache_exec_time,pipeline_exec_time,events,costs,configs"

while read LOG_LOCATION ; do
	LOG_FILE="$(basename $LOG_LOCATION)"
	
	#if [[ $LOG_FILE =~ ([^-])-(false|true)-(false|true)-(classic|precise)-(O0|O1).log ]] ; then
	if [[ $LOG_FILE =~ ([^-]*)-(false|true)-(false|true)-(classic|precise)-(O0|O1).log ]] ; then
		BENCH=${BASH_REMATCH[1]}
		INLINE=${BASH_REMATCH[2]}
		UNROLL=${BASH_REMATCH[3]}
		ANALYSIS=${BASH_REMATCH[4]}
		OPTIM=${BASH_REMATCH[5]}
		#echo "MATCH ! bench: $BENCH, INLINE: $INLINE, UNROLL: $UNROLL, analysis: $ANALYSIS, optim: $OPTIM"


		OUTPUT=$("$WCET_INFO_SCRIPT" --csv-summary "$LOG_LOCATION" /dev/null)
		echo "$BENCH,$INLINE,$UNROLL,$ANALYSIS,$OPTIM,$OUTPUT"
	else
		echo "WARNING ! Pattern $LOG_FILE not recognized"
	fi

done < <(printf '%s\n' "$LOG_LIST")
