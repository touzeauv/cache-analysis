#!/usr/bin/env python

import argparse
import re
import sys

parser = argparse.ArgumentParser(description='Extract useful information from Otawa log.')
parser.add_argument('log_file', metavar='LOG_FILE', help='Otawa log file (instruction log level) to extract data from')
parser.add_argument('csv', metavar='CSV_OUTPUT', help='Location of the output csv file produced')
parser.add_argument('--csv-summary', dest='csv_summary', default=False, action='store_true')

args = parser.parse_args()

if args.log_file == '-':
    input_file = sys.stdin
else:
    input_file = open(args.log_file)

matching_events = False;


class EdgeInfo:
    def __init__(self, cfg, edge):
        self.cfg = cfg
        self.edge = edge
        self.hts = None
        self.lts = None

        self.events = None
        self.cost_configs = 0

    def set_events(self, events):
        self.events = events

    def set_cost_configs(self, cost_configs):
        self.cost_configs = cost_configs

    def incr_cost_configs(self):
        self.cost_configs += 1

    def set_time_sets(self, hts, lts):
        self.hts = hts
        self.lts = lts

    def write_to_csv(self, csv_file):
        assert (self.hts != None and self.lts != None and self.cost_configs != None and self.events != None)
        csv_file.write('{0}|{1}|{2}|{3}|{4}|{5}\n'.format(self.cfg, self.edge, self.events, self.cost_configs, self.hts, self.hts - self.lts))


class ProgramInfo:
    def __init__(self, csv_summary):
        self.cache_analysis_exec_time = 0
        self.edge_time_builder_running_time = 0
        self.total_events = 0
        self.total_costs = 0
        self.total_config = 0
        self.wcet = 0
        self.analysis = {}
        self.csv_summary = csv_summary

    def add_cache_analysis_info(self, name, running_time):
        self.analysis[name] = running_time
        self.cache_analysis_exec_time += running_time

    def add_events(self, number_of_events):
        self.total_events += number_of_events
        self.total_config += 2**number_of_events

    def set_edge_time_builder_running_time(self, running_time):
        self.edge_time_builder_running_time = running_time

    def set_wcet(self, wcet):
        self.wcet = wcet

    def print_summary(self):
        if self.csv_summary:
            print '{0},{1},{2},{3},{4},{5}'.format(self.wcet, self.cache_analysis_exec_time, self.edge_time_builder_running_time, self.total_events, self.total_costs, self.total_config)
        else:
            print 'WCET: {0}s'.format(self.wcet)
            print 'Total cache analysis time: {0}s'.format(self.cache_analysis_exec_time)
            print 'Total edge event builder running time: {0}s'.format(self.edge_time_builder_running_time)
            print 'Total events: {0}\nTotal costs: {1}\nTotal config: {2}'.format(self.total_events, self.total_costs, self.total_config)


class RegexMatcher:
    def __init__(self):
        self.cfg_expr = re.compile('\s*process CFG (.*)')
        self.bb_expr = re.compile('\s*process (entry|BB \d*)\s*')
        self.edge_expr = re.compile('\s*(\S.*, .* -> .*, .*\S)\s*')
        self.dynamic_event_expr = re.compile('\s*dynamic events = ([0-9]*)\s*')
        self.cost_expr = re.compile('\s*cost = ([0-9]*)\s*')
        self.config_expr = re.compile('\s*\[\d*\] cost = .*')
        self.timesets_expr = re.compile('\s*LTS time = (\d*), HTS time = (\d*) for .*')
        self.cache_analysis_expr = re.compile('\s*(May|Must|Exist Hit|Exist Miss|Exact May|Exact Must) Analysis running time: ([0-9]*)\ss\s*')
	self.edge_time_builder_running_time_expr = re.compile('\s*Edge Time Builder running time: (\d*) s\s')
        self.edge_time_builder_begin_expr = re.compile('Starting otawa::etime::EdgeTimeBuilder (.*)\s*')
        self.edge_time_builder_end_expr = re.compile('Ending otawa::etime::EdgeTimeBuilder')
	self.wcet_expr = re.compile('\s*WCET = (.*)\s*')

        self.match = None

    def match_cfg(self, line):
        self.match = self.cfg_expr.match(line)
        return self.match

    def match_bb(self, line):
        self.match = self.bb_expr.match(line)
        return self.match

    def match_edge(self, line):
        self.match = self.edge_expr.match(line)
        return self.match

    def match_dynamic_event(self, line):
        self.match = self.dynamic_event_expr.match(line)
        return self.match

    def match_cost(self, line):
        self.match = self.cost_expr.match(line)
        return self.match

    def match_config(self, line):
        self.match = self.config_expr.match(line)
        return self.match

    def match_timesets(self, line):
        self.match = self.timesets_expr.match(line)
        return self.match

    def match_cache_analysis(self, line):
        self.match = self.cache_analysis_expr.match(line)
        return self.match

    def match_edge_time_builder_begin(self, line):
        self.match = self.edge_time_builder_begin_expr.match(line)
        return self.match

    def match_edge_time_builder_running_time(self, line):
        self.match = self.edge_time_builder_running_time_expr.match(line)
        return self.match

    def match_edge_time_builder_end(self, line):
        self.match = self.edge_time_builder_end_expr.match(line)
        return self.match

    def match_wcet(self, line):
        self.match = self.wcet_expr.match(line)
        return self.match


program = ProgramInfo(args.csv_summary)
regex_matcher = RegexMatcher()

with open(args.csv, 'w') as csv:
    csv.write('function|edge|#events|#costs|cost|variability\n')
    for line in input_file:

        if regex_matcher.match_cache_analysis(line):
            analysis_name = regex_matcher.match.group(1)
            analysis_running_time = int(regex_matcher.match.group(2));
            program.add_cache_analysis_info(analysis_name, analysis_running_time)

        elif regex_matcher.match_wcet(line):
            wcet = int(regex_matcher.match.group(1))
            program.set_wcet(wcet)

        elif regex_matcher.match_edge_time_builder_begin(line):
            matching_events = True

        elif regex_matcher.match_edge_time_builder_end(line):
            matching_events = False

        elif matching_events:

            if regex_matcher.match_cfg(line):
                cfg = regex_matcher.match.group(1)
                bb_in_cfg = 0

            elif regex_matcher.match_bb(line):
                bb_in_cfg += 1

            elif regex_matcher.match_edge(line):
                edge = EdgeInfo(cfg, regex_matcher.match.group(1))

            elif regex_matcher.match_dynamic_event(line):
                number_of_events = regex_matcher.match.group(1)
                program.add_events(int(number_of_events))
                edge.set_events(number_of_events)

            elif regex_matcher.match_config(line):
                edge.incr_cost_configs()

            elif regex_matcher.match_cost(line):
                edge.set_cost_configs(1)
                cost = int(regex_matcher.match.group(1))
                edge.set_time_sets(cost, cost)
                program.total_costs += 1
                edge.write_to_csv(csv)
            elif regex_matcher.match_timesets(line):
                lts = int(regex_matcher.match.group(1))
                hts = int(regex_matcher.match.group(2))
                program.total_costs += edge.cost_configs
                edge.set_time_sets(hts, lts)
                edge.write_to_csv(csv)

            elif regex_matcher.match_edge_time_builder_running_time(line):
                running_time = int(regex_matcher.match.group(1))
                program.set_edge_time_builder_running_time(running_time)

program.print_summary()

