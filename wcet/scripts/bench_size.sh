#!/bin/bash

BENCH_DIR="bounded-bench"
CACHE="hardware/cache/fully_associative_cache.xml"


if [ ! -d "$BENCH_DIR" ] ; then
	echo "Directory \"$BENCH_DIR\" does not exist"
	exit
fi

BENCH_LIST=$(find "$BENCH_DIR" -mindepth 2 -maxdepth 2 -type d)

echo "bench,lblocksO0,lblocksO1" > bench_size.csv

while read BENCH_LOCATION ; do
	BENCH=$(basename "$BENCH_LOCATION")
	LBLOCKS_O0=$(operform --add-prop otawa::CACHE_CONFIG_PATH=$CACHE -o /dev/null --log bb $BENCH_LOCATION/build/$BENCH-O0.elf main require:otawa::icat3::LBLOCKS_FEATURE 2>&1 | grep l-block | tail -n 1 | tr -d '\t' | cut -d ' ' -f 2)
	LBLOCKS_O1=$(operform --add-prop otawa::CACHE_CONFIG_PATH=$CACHE -o /dev/null --log bb $BENCH_LOCATION/build/$BENCH-O1.elf main require:otawa::icat3::LBLOCKS_FEATURE 2>&1 | grep l-block | tail -n 1 | tr -d '\t' | cut -d ' ' -f 2)
	echo "$BENCH,$LBLOCKS_O0,$LBLOCKS_O1" >> bench_size.csv
done < <(printf '%s\n' "$BENCH_LIST")

exit

