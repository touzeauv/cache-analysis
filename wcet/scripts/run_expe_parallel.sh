 #!/bin/bash

JOBS=16
BENCH_DIR="bounded-bench"
SCRIPT="cache_analysis.osx"
CACHE="hardware/cache/cache.xml"
PIPELINE="hardware/pipeline/toy.xml"
MEMORY="hardware/memory/lpc2138.xml"


check_exists()
{
	local FILE="$1"
	local ERROR="$2"
	if [ ! -f "$FILE" ] ; then
		echo "Error: $ERROR"
		exit
	fi
}

check_exists "$SCRIPT" "No Otawa script found"
check_exists "$CACHE" "No cache config found"
check_exists "$MEMORY" "No memory config found"
check_exists "$PIPELINE" "No pipeline config found"


if [ ! -d "$BENCH_DIR" ] ; then
	echo "Directory \"$BENCH_DIR\" does not exist"
	exit
fi


BENCH_LIST=$(find "$BENCH_DIR" -mindepth 2 -maxdepth 2 -type d)

echo "optim,unroll,inline,analysis,wcet,cache,pipeline,events,costs,configs" > result.log


while read OPTIM ; do
	while read INLINE ; do
		while read BENCH_LOCATION ; do
			mkdir -p "$BENCH_LOCATION/results"
		
			BENCH=$(basename "$BENCH_LOCATION")
		
			if [[ "$BENCH" != "insertsort" ]] ; then
				continue
			fi

			while read UNROLL ; do
				while read ANALYSIS ; do
					COMMANDS="owcet --log inst -f $BENCH_LOCATION/bounds/$BENCH-O$OPTIM.ffx -s $SCRIPT -p cache=$CACHE -p pipeline=$PIPELINE -p memory=$MEMORY -p cache_analysis=$ANALYSIS -p unroll=$UNROLL -p inline=$INLINE $BENCH_LOCATION/build/$BENCH-O$OPTIM.elf 2> $BENCH_LOCATION/results/$BENCH-$INLINE-$UNROLL-$ANALYSIS-O$OPTIM.log\n$COMMANDS"
				done < <(printf 'classic\nprecise\n')
			done < <(printf 'true\nfalse\n')
		done < <(printf '%s\n' "$BENCH_LIST")
	done < <(printf 'true\nfalse\n')
done < <(printf '0\n1\n')


echo -e "$COMMANDS" | parallel --jobs "$JOBS"

