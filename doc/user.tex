\documentclass[12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{fontenc}
\usepackage[english]{babel}
\usepackage{tikz}
\usepackage{hyperref}
\usepackage{listings}

\usepackage{filecontents}
\begin{filecontents*}{\jobname.bib}
@inproceedings{MayMustAnalysis,
	author    = {Martin Alt and Christian Ferdinand and Florian Martin and Reinhard Wilhelm},
	title     = {Cache Behavior Prediction by Abstract Interpretation},
	booktitle = {Static Analysis, Third International Symposium, SAS'96, Aachen, Germany, September 24-26, 1996, Proceedings},
	pages     = {52--66},
	year      = {1996},
	crossref  = {SAS96},
	url       = {https://doi.org/10.1007/3-540-61739-6\_33},
	doi       = {10.1007/3-540-61739-6\_33},
	timestamp = {Wed, 11 Apr 2018 14:30:26 +0200},
	biburl    = {https://dblp.org/rec/bib/conf/sas/AltFMW96},
	bibsource = {dblp computer science bibliography, https://dblp.org}
}
@proceedings{SAS96,
	editor    = {Radhia Cousot and David A. Schmidt},
	title     = {Static Analysis, Third International Symposium, SAS'96, Aachen, Germany, September 24-26, 1996, Proceedings},
	series    = {Lecture Notes in Computer Science},
	volume    = {1145},
	publisher = {Springer},
	year      = {1996},
	url       = {https://doi.org/10.1007/3-540-61739-6},
	doi       = {10.1007/3-540-61739-6},
	isbn      = {3-540-61739-6},
	timestamp = {Mon, 22 May 2017 16:14:04 +0200},
	biburl    = {https://dblp.org/rec/bib/conf/sas/1996},
	bibsource = {dblp computer science bibliography, https://dblp.org}
}
@article{POPL_paper,
	author    = {Valentin Touzeau and Claire Ma{\"{\i}}za and David Monniaux and Jan Reineke},
	title     = {Fast and exact analysis for {LRU} caches},
	journal   = {{PACMPL}},
	volume    = {3},
	number    = {{POPL}},
	pages     = {54:1--54:29},
	year      = {2019},
	url       = {https://dl.acm.org/citation.cfm?id=3290367},
	timestamp = {Tue, 22 Jan 2019 15:27:56 +0100},
	biburl    = {https://dblp.org/rec/bib/journals/pacmpl/TouzeauMMR19},
	bibsource = {dblp computer science bibliography, https://dblp.org}
}
@inproceedings{CAV_paper,
	author    = {Valentin Touzeau and Claire Ma{\"{\i}}za and David Monniaux and Jan Reineke},
	title     = {Ascertaining Uncertainty for Efficient Exact Cache Analysis},
	booktitle = {Computer Aided Verification - 29th International Conference, {CAV} 2017, Heidelberg, Germany, July 24-28, 2017, Proceedings, Part {II}},
	pages     = {22--40},
	year      = {2017},
	crossref  = {CAV2017-2},
	url       = {https://doi.org/10.1007/978-3-319-63390-9\_2},
	doi       = {10.1007/978-3-319-63390-9\_2},
	timestamp = {Fri, 14 Jul 2017 13:10:55 +0200},
	biburl    = {https://dblp.org/rec/bib/conf/cav/TouzeauMMR17},
	bibsource = {dblp computer science bibliography, https://dblp.org}
}
@proceedings{CAV2017-2,
	editor    = {Rupak Majumdar and Viktor Kuncak},
	title     = {Computer Aided Verification - 29th International Conference, {CAV} 2017, Heidelberg, Germany, July 24-28, 2017, Proceedings, Part {II}},
	series    = {Lecture Notes in Computer Science},
	volume    = {10427},
	publisher = {Springer},
	year      = {2017},
	url       = {https://doi.org/10.1007/978-3-319-63390-9},
	doi       = {10.1007/978-3-319-63390-9},
	isbn      = {978-3-319-63389-3},
	timestamp = {Fri, 14 Jul 2017 13:04:29 +0200},
	biburl    = {https://dblp.org/rec/bib/conf/cav/2017-2},
	bibsource = {dblp computer science bibliography, https://dblp.org}
}
\end{filecontents*}

\bibliographystyle{apalike}

\newcommand{\software}[1]{\textsc{#1}}

\title{User guide}
\author{Valentin Touzeau}

\begin{document}
\maketitle

\begin{section}{\software{Otawa} basics}

\begin{subsection}{Processors}

\software{Otawa} is a framework for Worst-Case Execution Time estimation that consists in both tools and libraries.
Similarly to a compiler it is organized into passes, called \emph{processors} in the \software{Otawa} terminology.
Ideally, each processor is responsible for a single atomic task (parsing a binary executable, building a CFG, building an ILP, etc.) and no other.
The \software{Otawa} core is then responsible for scheduling the needed processors in the correct order.

As a first approximation, one can see an \software{Otawa} plugin as a collection of processors.
Loading a plugin then makes its processors available for analysis.
To distinguish between processors provided by different module and to avoid name clashing, the name of processors are prefixed by the namespace they belong to.
For example, the processor \texttt{otawa::LoopUnroller} (responsible for unrolling the first iteration of loops) can be found in the \software{Otawa} core.
Similarly, many executables provided by \software{Otawa} (oipet, owcet, operform, etc.) roughly consist in wrapping a set of processors in a user-friendly interface.

\end{subsection}

\begin{subsection}{Features}

To ease the scheduling of processors, dependencies between two processors are organized into \emph{features}.
One way to understand features is to think about them as abstract entities representing the input/outputs relation between processors.
For example, the processor \texttt{otawa::LoopUnroller} provides the feature \texttt{otawa::UNROLLED\_LOOPS\_FEATURE}.
Conversely, it requires 4 features: \texttt{otawa::DOMINANCE\_FEATURE}, \texttt{otawa::LOOP\_HEADERS\_FEATURE}, \texttt{LOOP\_INFO\_FEATURE} and \texttt{COL:ECTED\_CFG\_FEATURE}.
By convention, the name of a feature is capitalized, prefixed by the namespace it belongs to and suffixed by FEATURE.
Note that this situation, where one processor requires several features but provides only one is really common.
Indeed, because processors are designed to accomplish a single task, they usually provide a single feature.
However, they might use information coming from different features.
For example, many processors run on a program CFG, which is one feature, and need additional decoration provided by another feature.

Every feature has a default processor associated to it that can be used to provides the feature.
This way, when the user ask for a feature, \software{Otawa} is able to recursively build the DAG of dependencies between processors and features to process them in order and provide the desired feature without other information.
In addition, the processor/feature system makes \software{Otawa} flexible.
For example, if someone design a new algorithm for unrolling loops, it can implement it in a processor that provides the \texttt{otawa::UNROLLED\_LOOPS\_FEATURE}.
By indicating to \software{Otawa} that this new processor should be used when the \texttt{otawa::UNROLLED\_LOOPS\_FEATURE} is required, the correct chain of processor will be executed without modifying the processors depending on the feature.

\end{subsection}

\begin{subsection}{Extracting WCET using \texttt{owcet}}


\texttt{owcet} is the tool used to extract a WCET estimation from a ARM binary.
The following command gives an example of a simple usage of \texttt{owcet}.
\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
$ owcet -s script.osx executable.elf main
\end{lstlisting}
\texttt{script.osx} is an xml file describing the processors and features used to compute the WCET.
It is used to describe the architecture running the analyzed binary and provide the list of processors to use to erform the analysis.
\texttt{executable.eld} is the binary to analyze (\software{Otawa} supports \software{ARM}, \software{MIPS} and \software{PowerPC} assembly).
It is recommended to compile this binary with the debug symbols.
\texttt{main} is the function we want to compute the WCET of.

The vast majority of programs contain loops which number of iteration must be bounded in order to extract a WCET estimation.
In many cases, bounding loops iteration can be done by using tool called \texttt{orange} which works at the source level and match the program against known patterns.
The following example gives an example of command line call to \texttt{orange}:
\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
$ orange main.c util.c lib.c main -o bounds.ffx
\end{lstlisting}
In this example, \texttt{main.c}, \texttt{util.c} and \texttt{lib.c} are the program source file, \texttt{main} is the program entry point, and \texttt{bounds.ffx} is a file (called flowfacts file) containing a human-readable high-level AST of the program in a XML format, decorated by the deduced bounds.
Note that some intricate nested loop might be left unbounded by \texttt{orange}.
In this case, missing bounds have to be manually filled.
The flowfacts can then be provided to \texttt{owcet} using the \texttt{-f} option.
Remember that the binary provided to \texttt{owcet} must be compiled with debug information for \software{Otawa} to match ``software loops'' against ``binary loops''.

A usual compilation command-line looks like the following:
\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
$ arm-none-eabi-gcc --specs=nosys.specs -g -o executable.elf src/*.c
\end{lstlisting}


Note that even in a case where \texttt{orange} is able to bound all loops at C level, some loops might still be unbounded.
First, sources of libraries are not always available.
In addition, compiler can introduce loops into the binary.
For example, in some cases, like complex floating-point operations (like multiplication or division) with no hardware support, the compiler replaces the operation by a set of instruction that performs an equivalent computation but might contain loops.
In these situation, one can build a flowfacts file from the binary using \texttt{makeff}.
Once filled by hand, this file might be used by \texttt{owcet} as usual.

In addition to loop bounds, the script provided to \texttt{owcet} through the \texttt{-s} option might allow the user to provide additional information to tune the analysis.
For example, the script \texttt{generic.osx} (found at \texttt{/usr/local/share/Otawa/scripts/} in my installation) provides a parameter \texttt{cache} to define a cache configuration file.
An example of such cache configuration file can be found in \texttt{/ust/local/share/Otawa/caches/inst-64x16x1.xml}.

Finally, the output of \texttt{owcet} is usually minimal and simply consist of the actual WCET estimation.
In case something unexpected happened, one can obtain additional information using the \texttt{--log} option (see \texttt{owcet -h} for details).

An example of \texttt{owcet} command-line thus looks like:
\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
$ owcet --log bb -s script.osx -p cache=inst-64x16x1.xml -f bounds.ffx executable.elf main
\end{lstlisting}



\end{subsection}

\end{section}

\begin{section}{Overview of the modules}

We developped 5 \software{Otawa} modules related to cache analyses:
\begin{itemize}
	\item \software{lrupreanalysis}: this module implements corrected version of the may and must analyses\cite{MayMustAnalysis} and the definitely unknown analysis presented in~\cite{CAV_paper}.
	\item \software{lruexact}: this module is used to compute the set of memory accesses that can be refined by an exact analysis.
	\item \software{lrumc}: this module contains the analyses required to establish an exact classification using the model-checking approach described in~\cite{CAV_paper}.
	\item \software{lruzdd}: similar to \software{lrumc}, this module performs an exact classification using the ZDD approach described in~\cite{POPL_paper}.
	\item \software{lrusecurity}: this modules contains the processors related to security (in particular, the tracking of blocks last loading location).
\end{itemize}

\begin{figure}
	\centering
	\begin{tikzpicture}
		\node[draw,rectangle] (icat)           at (0, 0) {otawa::icat3};
		\node[draw,rectangle] (icache)         at (4, 0) {otawa::icache};
		\node[draw,rectangle] (etime)          at (8, 0) {otawa::etime};
		\node[draw,rectangle] (lrupreanalysis) at (2, 2) {lrupreanalysis};
		\node[draw,rectangle] (lruexact)       at (6, 2) {lruexact};
		\node[draw,rectangle] (lrusecurity)    at (0, 4) {lrusecurity};
		\node[draw,rectangle] (lrumc)          at (4, 4) {lrumc};
		\node[draw,rectangle] (lruzdd)         at (8, 4) {lruzdd};

		\draw[-latex] (lrupreanalysis) -- (icat);
		\draw[-latex] (lrupreanalysis) -- (icache);
		\draw[-latex] (icat)           -- (icache);
		\draw[-latex] (lruexact)       -- (lrupreanalysis);
		\draw[-latex] (lrusecurity)    -- (lrupreanalysis);
		\draw[-latex] (lrumc)          -- (lruexact);
		\draw[-latex] (lruzdd)         -- (lruexact);
		\draw[-latex] (etime)          -- (lruexact);

		\draw[dashed] (0, 1) -- (8, 1);
	\end{tikzpicture}
	\caption{modules dependencies}
	\label{fig:modules_interactions}
\end{figure}

The interaction between the modules and \software{Otawa} is described on figure~\ref{fig:modules_interactions}.
\software{otawa::icache} first split basic blocks according to the cache blocks (or l-blocks in \software{Otawa} terminology) boundaries.
The \software{otawa::icat3} module is then decorating the CFG with memory accesses, one for each sub-block obtained, and map them to their associated l-blocks.
These decoration are used in \software{lrupreanalysis} modules to perform may, must, exist-hit and exist-miss analyses.
\software{lrupreanalysis} also provides a classification of memory accesses into \emph{Always-Hit}, \emph{Always-Miss}, \emph{Definitely Unknown} and \emph{Not Classified} according to these analyses.
The results of may and exist-hit analyses can then be used in \software{lrusecurity} to find the source of potential information leakage.
The classification provided by the \software{lrupreanalysis} can also be used by the \software{lruexact} module to build a list of memory accesses that can be refined by a more precise analysis.
This list is then used by \software{lrumc} and \software{lruzdd} to perform an exact classification of the accesses left unclassified (either relying on a model checker approach~\cite{CAV_paper}, or by abstract interpretation as described in~\cite{POPL_paper}).
All analysis steps are optional, and \software{lruexact} can convert all classifications to pipeline events as used in \software{otawa::etime}.

\end{section}

\begin{section}{Installation}
	\begin{subsection}{Installing \software{otawa}}
		There is no release of \software{otawa}.
		This means that \software{otawa} sources must be downloaded and compiled to have a working installation.
		Hopefully, a script can be used to perform the download and installation for you.
		The first step of otawa installation is thus to download \emph{otawa-install.py} from \url{otawa.fr}.
		Before executing the script, you need to install a C++ compiler, several version manager and some other libraries \software{otawa} depends on.
		A typical pre-install is thus

		\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
		$ sudo apt install g++ cmake make mercurial git flex bison libxml2-dev libxslt1-dev ocaml
		\end{lstlisting}

		Then, \software{otawa} can be installed by running the script.
		\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
		$ ./otawa-install.py -B src
		\end{lstlisting}
		Note that the \emph{-B} option is not mandatory.
		It is used to indicate to the script where the sources should be downloaded and compiled.
		By default, the build directory is \emph{/tmp/}.
		By overwriting it, one can easily patch the sources if needed.

		In order to extract WCET from binaries, some modules might be necessary.
		One can obtain a list of the available modules by running \emph{otawa-install.py -l}.
		In our case, we install \emph{otawa-arm}, \emph{orange} and \emph{otawa-lp\_solve5} which are respectively used to parse ARM binaries, extract loop bounds from C sources, and solve ILP problems to extract WCET.

		\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
		$ ./bin/otawa-install.py -B src otawa-arm otawa-lp_solve5 orange
		\end{lstlisting}

		Note that this second step is done by using the script located in \emph{bin}, and not the one you downloaded (which can be removed).
		On the script termination, 4 directories have been created: \emph{bin}, \emph{share}, \emph{include} and \emph{lib}.
		They can thus be copied under \emph{/usr/local/} to finish the \software{otawa} install.

		\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
		$ sudo cp -r bin/* /usr/local/bin/
		$ sudo cp -r include/* /usr/local/include/
		$ sudo cp -r lib/* /usr/local/lib/
		$ sudo cp -r share/* /usr/local/share/
		\end{lstlisting}
	\end{subsection}

	\begin{subsection}{Installing cache analysis plugins}
		Because \software{otawa} does not provide any stable release, newer version might not be compatible with the cache analysis plugin.
		This guide assumes that the following version of the different parts of \software{otawa}:
		\begin{itemize}
			\item armv7t: hg commit c5376e7be766844a1569b5e38e79bfe49eb70b44 from 2018-01-15 16:02
			\item elm: git commit c2b416db19f4971a0b83a66191aa652be894f8d8 from 2019-08-22 09:58
			\item frontc: hg commit 62019eeece77c12be33169dcd989a4f01de7d15f from 2018-01-09 09:59
			\item gel: git commit 975323fb110ad04efebc50b28976258cd3d4cf39 from 2019-04-09 17:29
			\item gliss2: hg commit e6025590905230ccc6ae872d90a6aceb113de41c from 2019-07-07 22:08
			\item lpsolve5: tarball shasum: f7e4c56ba79eeedb659cf6537645ab21202b30c8 version 5.5.0.10
			\item orange: hg commit 5d3ce6c0449ace5d78c05ff1cd34b9de70f5607b from 2017-08-24 11:38
			\item otawa: git commit f928c6c7dee453cfbc302c4dbd21f476010bb298 from 2019-08-20 15:19
			\item otawa-arm: git commit e56180f7847518495ea2b58885e8fbf840bf89c2 from 2019-05-29 16:07
			\item otawa-lp\_solve5: git commit a3b4c5d4a76f99fedc980220f7bb26929ea93561 from 2019-07-17 11:19
		\end{itemize}

		One can then clone the main repository of the project:

		\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
		$ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/touzeauv/cache-analysis.git
		$ cd cache-analysis
		$ git submodule update --init
		\end{lstlisting}

		In addition to \software{otawa} itself, the plugin \software{lruzdd} requires additional packages in order to compile \software{Cudd} (the decision digram library it relies on).
		If you want to use \software{lruzdd}, you thus need to install autoconf and libtool:

		\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
		$ sudo apt install autoconf libtool
		\end{lstlisting}

		Once this is done, one can compile all the modules one by one (the compilation order matters, as some plugins are dependencies of some others) using the usual CMake process.
		\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
		$ cd ~/cache-analysis/dependencies/lrupreanalysis
		$ mkdir build
		$ cd build
		$ cmake ..
		$ make
		$ sudo make install
		$ cd ~/cache-analysis/dependencies/lruexact
		$ mkdir build
		$ cd build
		$ cmake ..
		$ make
		$ sudo make install
		$ cd ~/cache-analysis/dependencies/lrumc
		$ mkdir build
		$ cd build
		$ cmake ..
		$ make
		$ sudo make install
		$ cd ~/cache-analysis/dependencies/lruzdd
		$ mkdir build
		$ cd build
		$ cmake ..
		$ make
		$ sudo make install
		$ cd ~/cache-analysis/dependencies/lrusecurity
		$ mkdir build
		$ cd build
		$ cmake ..
		$ make
		$ sudo make install
		\end{lstlisting}

		One can check at anytime the plugins availables by running otawa-config:
		\begin{lstlisting}[language=bash,basicstyle=\footnotesize\ttfamily,columns=fullflexible]
		$ otawa-config -V --plug
		\end{lstlisting}

	\end{subsection}
\end{section}

\begin{section}{Plugin details}
	\begin{subsection}{\software{lrupreanalysis}}
		The \software{lrupreanalysis} plugin is used to perform the fast analyses pruning the state space of the more costly exact analyses.
		\begin{itemize}
			\item \emph{may\_must::MustAnalysis}: Provides a debugged version of the naive \software{otawa::icat3} implementation of the must analysis described in~\cite{MayMustAnalysis}.
			\item \emph{may\_must::MayAnalysis}: Idem, provides the may analysis
			\item \emph{eh\_em::ExistHitAnalysis}: Provides the Exist-Hit analysis described in~\cite{CAV_paper}.
			\item \emph{eh\_em::ExistMissAnalysis}: Idem
		\end{itemize}

		\begin{figure}[ht]
			\centering
			\includegraphics[width=\textwidth]{diagrams/lrupreanalysis.pdf}
			\caption{Overview of the \software{lrupreanalysis} plugin}
			\label{fig:lrupreanalysis}
		\end{figure}

		Figure~\ref{fig:lrupreanalysis} shows the interactions between the processors (ellipsis) and features (rectangle box) of the \software{lrupreanalysis}.
		Elements in red are processors and features that do not belong to the plugin.
		This includes \emph{icat3::MayAnalysis} and \emph{icat3::MustPersAnalysis} that perform the usual may and must analyses.
		The \emph{icat3::CatBuilder} processor then use these results to classify memory accesses as Always-Hit, Always-Miss, First-Miss or Unknown.
		Note that the dependency of \emph{icat3::CatBuilder} on \emph{icat3::MAY\_ANALYSIS\_FEATURE} is optional: \emph{icat3::CatBuilder} can perform its classification without running \emph{icat3::MayAnalysis} (in which case no access is classified as Always-Miss).

		Conversly, elements in green are the one provided by the plugin.
		Because some bugs have been found in the implementation of May and Must analyses, they have been re-implemented in \software{lrupreanalysis}.
		In addition, the Must analysis has been decorelated from the persistence analysis, and only computes the \emph{icat3::MUST\_IN} property.
		Note that it still provides the \emph{icat3::MUST\_PERS\_ANALYSIS\_FEATURE} to avoid running the buggy analyses through the dependencies resolution.

		We provide the \emph{lrupreanalysis::may\_must::MAY\_MUST\_CATEGORY\_FEATURE} to perform the \emph{icat3::CATEGORY\_FEATURE} with the May analysis.

		Finally, the plugin provide processors to perform the Exist-Hit and Exist-Miss analyses, and the corresponding classification.
	\end{subsection}
	\begin{subsection}{\software{lruexact}}
		The \software{lruexact} plugin is organized around a main feature called \emph{lruexact::LRU\_CLASSIFICATION\_FEATURE}, and provides two kinds of processors: some for classifying accesses to refine before performing a costly exact analysis, others running after the exact analysis to translate the classification to pipeline events used to extract WCET of basic blocks.
		\begin{itemize}
			\item \emph{RefinementCatBuilder}: Check which memory accesses can be refined, and which one are already classified.
			\item \emph{DefaultClassificationBuilder}: Provides a default classification where all accesses are unknown.
			\item \emph{MayMustClassificationBuilder}: Provides a classification of accesses based on the May and Must analysis alone.
			\item \emph{ClassificationDisplayer}: Display the \emph{lruexact::LRU\_CLASSIFICATION} property either on the standard output, or in a file.
			\item \emph{EdgeEventBuilder}: Translates the \emph{lruexact::LRU\_CLASSIFICATION} into events usable by the pipeline analysis.
		\end{itemize}

		\begin{figure}[ht]
			\centering
			\includegraphics[width=\textwidth]{diagrams/lruexact.pdf}
			\caption{Overview of the \software{lruexact} plugin}
			\label{fig:lruexact}
		\end{figure}

		Figure~\ref{fig:lruexact} shows the interactions between processors and features related to the \software{lrupreanalysis} plugin.
		Based on the classification given by \emph{icat3::CATEGORY\_FEATURE} and \emph{lrupreanalysis::eh\_em::DU\_CATEGORY} (if available), \software{lruexact} split accesses into the one that are already classified or potentially Always-Hit or Always-Miss.
		This analysis can then be used by exact analysis provided by \software{lruzdd} or \software{lrumc} to avoid useless work.
		The \emph{LRU\_CLASSIFICATION\_FEATURE} can also be provided by coarser analyses.

		It can then be displayed on standard input or in a file (by setting \emph{CLASSIFICATION\_TO\_FILE} to true and indicating the filename in \emph{CLASSIFICATION\_PATH}) and used by \emph{EdgeEventBuilder} to build pipeline events.
	\end{subsection}
	\begin{subsection}{\software{lruzdd}}
		The \software{lruexact} plugin provides exact analyses based on Zero-suppressed Decision Diagrams.
		Analyses can be characterized according to two criteria: May vs Must analyses, and Global analyses vs Local analyses.
		Here, we used the term local to designate an analysis that reset the \software{cudd} environment between the analyses of two memory accesses to the same lblock to save memory, and global to designate an exact analysis that can share diagrams between analyses of the same lblocks\footnote{Unfortunately, one can note perform the analysis of different lblocks without resetting the cudd environment. Indeed, changing the lblock focus changes the update function and invalidate previous results that have been memoïzed}.
		One can thus find the following processors in the \software{lruexact} plugin:
		\begin{itemize}
			\item \emph{MayAnalysis}: An analysis that find all Always-Miss accesses.
			\item \emph{GlobalMayAnalysis}: Idem, with ressource sharing between analyses of different accesses to a common lblock.
			\item \emph{MustAnalysis}: An analysis that find all Always-Hit accesses.
			\item \emph{GlobalMustAnalysis}: Idem, with ressource sharing.
			\item \emph{ClassificationBuilder}: A final classification of memory accesses that does not leave any access Unknwon.
		\end{itemize}

		\begin{figure}[ht]
			\centering
			\includegraphics[width=\textwidth]{diagrams/lruzdd.pdf}
			\caption{Overview of the \software{lruzdd} plugin}
			\label{fig:lruzdd}
		\end{figure}

		Figure~\ref{fig:lruzdd} is straight forward.
		All analyses rely on the knowledge of lblocks, cfgs and accesses to refine, and flag accesses as Always-Hit or Always-Miss.
		Finally, one processor is used to combined these flags into a classification.
	\end{subsection}
	\begin{subsection}{\software{lrumc}}
		The \software{lruexact} plugin provides exact analyses based on Zero-suppressed Decision Diagrams.
		Analyses can be characterized according to two criteria: May vs Must analyses, and Global analyses vs Local analyses.
		Here, we used the term local to designate an analysis that reset the \software{cudd} environment between the analyses of two memory accesses to the same lblock to save memory, and global to designate an exact analysis that can share diagrams between analyses of the same lblocks\footnote{Unfortunately, one can note perform the analysis of different lblocks without resetting the cudd environment. Indeed, changing the lblock focus changes the update function and invalidate previous results that have been memoïzed}.
		One can thus find the following processors in the \software{lruexact} plugin:
		\begin{itemize}
			\item \emph{MayAnalysis}: An analysis that find all Always-Miss accesses.
			\item \emph{GlobalMayAnalysis}: Idem, with ressource sharing between analyses of different accesses to a common lblock.
			\item \emph{MustAnalysis}: An analysis that find all Always-Hit accesses.
			\item \emph{GlobalMustAnalysis}: Idem, with ressource sharing.
			\item \emph{ClassificationBuilder}: A final classification of memory accesses that does not leave any access Unknwon.
		\end{itemize}

		\begin{figure}[ht]
			\centering
			\includegraphics[width=\textwidth]{diagrams/lruzdd.pdf}
			\caption{Overview of the \software{lruzdd} plugin}
			\label{fig:lruzdd}
		\end{figure}

		Figure~\ref{fig:lruzdd} is straight forward.
		All analyses rely on the knowledge of lblocks, cfgs and accesses to refine, and flag accesses as Always-Hit or Always-Miss.
		Finally, one processor is used to combined these flags into a classification.
	\end{subsection}
\end{section}
\begin{section}{Virtual machine}
	A virtual machine with complete installation is available.
	The username/password couple is otawa/otawa, and the root password is otawa.
	In the home directory of the otawa user, one can find the following directory:
	\begin{itemize}
		\item otawa: An installation of the otawa framework, done as mentionned in this documentation.
In addition, a slight modification as been performed on the EdgeTimeBuilder processor, to output the total running time of this processor.
This is used to compared the running time of the pipeline and cache analyses.
		\item cache-analysis: an installation of the cache analysis project described here.
All plugins are already compiled and installed.
		\item test: a directory containing example of script to use the analyses of the cache analysis project.
In particular, it contains:
		\begin{itemize}
			\item A directory bounded-bench containing the tacle benchmarks, their compiled version, and the associated flowfacts.
			\item A directory hardware containing XML configuration files (for memory, pipeline and cache) usable by owcet.
			\item A owcet script cache\_analysis.osx used by owcet to extract a WCET.
			\item A bash script run\_experiment\_parallel.sh that runs analyze all the benchmark in the bounded-bench directory and store.
			\item A bash script summarize\_all.sh that reads the log file produced by run\_experiments\_parallel.sh and extract analysis time information to store them in a csv file.
		\end{itemize}
	\end{itemize}
\end{section}


\begin{section}{Bibliography}
	\bibliography{\jobname}
\end{section}

\end{document}

