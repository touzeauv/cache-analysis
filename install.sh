#!/bin/bash

git submodule update --init

red='\033[0;31m'
green='\033[0;32m'
blue='\033[0;36m'
nc='\033[0m'

display_message () {
	local message="$1"
	local color="$2"
	echo -e "${color}INSTALL SCRIPT: ${message}${nc}"
}

fail () {
	local error="$1"
	display_message "${error}" "${red}"
	exit
}

build_module () {
	local location=$(pwd)
	local module="$1"
	local module_folder="dependencies/$module"

	display_message "Build and install module ${module}" "${blue}"
	cd "${module_folder}"
	mkdir -p build || fail "Error: Cannot create build directory for module ${module}"
	cd build
	cmake .. || fail "Error running \"cmake ..\" for module ${module}"

	display_message "Building module ${module}..."
	make && display_message "Module ${module} build" "${green}" || fail "Error running \"make\" for module ${module}"
	display_message "Installing module ${module}..."
	sudo make install && display_message "Mdule ${module} installed" "${green}"

	cd "${location}"
}

build_module "lrupreanalysis"
build_module "lruexact"
build_module "lrumc"
build_module "lruzdd"
build_module "lrusecurity"

#lrupreanalysis
#lruexact
#lrumc
#lruzdd
#lrusecurity
#ocache
#
#cd dependencies/module/
#mkdir build
#cd build
#cmake ..
#make
#sudo make install


